---
gitea: none

include_toc: true

---

[TOC]

# Cyber

## S3 IDN

1. Acteurs sur Internet (cf PDF)
2. Bases du réseau
3. Biométrie
4. Biométrie, limites, attaques et protections
5. [Protocoles d'authentification](./IDN/5-Authentification.md)
6. [Systèmes d'identités](./IDN/6-Systèmes-Identités.md)

# Web

## S3 Technologies Web

1. <mark>[B-archi]</mark> HTML [/ architecture projet]

2. <mark>[G]</mark> CSS

3. <mark>[E]</mark> Brython (client-side) - [API JS-DOM + WebComponents]

4. <mark>[A]</mark> Python (serveur)
   
   1. [Communications Client-Serveur](./Web/X%20-%20Communications%20Client-Serveur/index.md)
      
      - [ ] Le serveur HTTP
        
        - [ ] Routes {name} cf Moodle
      
      - [ ] WebSocket
      
      - [ ] Move optimisation 2 [F]
        
        - [ ] OPTI REST API (cf youtube)

5. <mark>[F]</mark> Perfs et SEO - [partie opti de com' Client-Serveur]

## S4 Programmation Web

1. <mark>[D-TS]</mark> JS (+ TS type - basique)
   
   1. Manque structures de données [Obj/Array / Str / Map/Set]

2. Classes, fonctions avancées, imports (30min) + APIs (30min)

3. [X] WebComponents en vrai / Animations / transitions => Setinter+animationFR/ canvas / WebWorker/service workers + socket servers [Add fiche] + [Worklet - Web APIs | MDN](https://developer.mozilla.org/en-US/docs/Web/API/Worklet) / [Animation Worklet - HTTP203 Advent - YouTube](https://www.youtube.com/watch?v=ZPkMMShYxKU&t=19s)

4. [<mark>D-TS</mark>] TS avancé (dont TS config ?)

5. unit tests / tests de recettes (en pratique) / validation des données runtime

Web3A : WebPack / CI/CD / SEO ???

## S5 .....

1. Chart.js...

## Unsorted

- [JavaScript](./Web/X%20-%20JavaScript/index.md)

- [API JS-DOM](./Web/X%20-%20API%20JS-DOM/index.md)
