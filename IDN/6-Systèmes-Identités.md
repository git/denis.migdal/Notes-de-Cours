---
gitea: none

include_toc: true

---

[TOC]



Un même utilisateur peut s'authentifier auprès de plusieurs services différents, parfois appartenant à la même organisation. Par exemple, au sein de l'UCA, vous utilisez différents services :

- l'ENT ;

- Moodle ;

- l'emploi du temps ;

- les mails ;

- l'application de suivi des stages ;

- le suivi des notes ;

- le suivi des absences ;

- etc.



Si vous deviez avoir un compte différent pour chacun de ces services, cela deviendrait très vite ingérable avec des problèmes de synchronisations. Imaginez donc avoir à retenir au moins 7 mots de passes différents rien que pour l'UCA, ou d'avoir à changer une information sur vos comptes avec le risque d'en oublier un.



Pour résoudre ce problème on utilise des **fournisseurs d'identités** (Identity Provider, IP/IdP). L'IP est l'entité qui gérera votre compte, i.e. l'authentification et les informations liées à votre compte.



On distingue généralement 4 architectures :

- **une gestion isolée** : le cas le plus simple où le SP gère lui-même votre compte.

- **une gestion centralisée** : un IP gère votre compte pour plusieurs SP.

- **une gestion centrée utilisateur** : vous hébergez vous-même votre propre IP localement.

- **une gestion fédérée** : un ensemble d'IP permettent de se connecter à plusieurs SP.



Pour des raisons de sécurité, on part généralement du principe que les différents acteurs (ici le client, l'IP, et le SP) ne se font pas confiance et peuvent être soit malhonnêtes, soit corrompus.



# La sécurité des données stockée



-> IP stocke données et transmets aux SP. -> auth. SP vérifier s'il a accès aux données.

-> SP -> IP ==> soit stocké localement (pas partagés), soit chiffré sym. (conf.) potentiellement avec une clé partagée avec d'autres SP (ou sur de la crypto multi-parties - e.g. chiffrer les clés symétriques), soit signé/hashé pour garantir l'intégrité.



Idem client vs SP données (e.g. stockage/partage de données) : dérivé du MdP clé privée, pour chiffrer une clé asym permettant de stocker des données chiffrées, déchiffrée sur le client.



# Jetons d'accès (token access)

Preuve auth. IP => SP, passe par le client (éviter preuve passe par SP).

+ SP => IP pour accès aux données
- pas confiance IP

- unlikability.



# Futur

- 7 lois d'ID

- SSO

- Unlikability
