---
gitea: none

include_toc: true

---

# Typescript

[TOC]

## JavaScript et TypeScript

Au semestre précédent, nous avons utilisé Brython afin d'exécuter du Python dans le navigateur. En réalité Brython transforme le code Python en JavaScript qui est ensuite interprété par le navigateur. En effet, le navigateur ne peut exécuter que deux langages :

- **WASM** : une sorte de langage bas niveau vers lequel d'autres languages peuvent être compilés (e.g. C, C++, Rust, Java, etc).

- **JavaScript**

JavaScript signale uniquement les erreurs d'exécutions. Ainsi certaines erreurs de programmations peuvent être silencieuses, ou ne pas être détectées lors de vos tests. Nous utiliserons TypeScript, un sur-ensemble de JavaScript permettant de détecter des erreurs en amont, facilitant grandement les étapes de tests et de développements.

Typescript permet d'indiquer le type des variables permettant à la fois de documenter l'usage des fonctions, mais aussi de vérifier et garantir qu'une fonction est bien appelée avec les bons paramètres.  Ainsi en à peine quelques millisecondes, Typescript est capable de vous avertir d'erreurs de programmations que vous pourriez ne pas détecter, puis passer une journée à chercher. Il facilite aussi grandement les étapes de refactoring, e.g. lorsque la signature d'une fonction change, et requiert alors de changer son appel dans l'intégralité de votre code.

Au semestre précédent, nous avons utilisé l'API JS/DOM via Brython. Nous utiliserons la même API ce semestre, i.e. vous réutiliserez les mêmes fonctions et paramètres, seul le langage utlisé change.

### Exécution de Typescript côté client

Le navigateur ne peut pas exécuter directement du code TypeScript. Il faut ainsi, en amont, le convertir en JavaScript à l'aide de la commande shell `tsc` :

```bash
tsc --target esnext --strict --watch $FILE
```

Les options sont soit données en paramètre de la commande, ou indiquées dans un fichier `tsconfig.json`. L'option `--watch` permet de générer les fichiers JS à chaque modifications du fichier TS correspondant. Son avantage majeur est d'intégrer un cache en RAM permettant d'éviter de re-générer les fichiers JS de fichiers TS non-modifiés. Il permet ainsi un gain de temps significatif lors du développement, passant de plusieurs minutes de traduction pour de gros projets, à quelques millisecondes.

Une fois le fichier JS généré, on peut désormais l'inclure à la page Web :

```html
<!DOCTYPE html>
<html>
    <head>
        <!-- ... -->
        <script src="$JS_FILE" type="module" defer></script>
    </head>
    <body>
    </body>
</html>
```

💡 Dans le cadre d'un projet, il est recommandé d'utiliser un `tsconfig.json` :

```shell
tsc --init --target esnext --strict # initialiser un projet (crée le tsconfig).
tsc --build --watch # convertir les fichiers TS du projet en JS.
```

#### Installation de `tsc`

```shell
npm install typescript --save-dev
echo "PATH=\$PATH:$PWD/node_modules/typescript/bin/" >> ~/.bashrc
```

### Exécution de TypeScript côté serveur

JavaScript et TypeScript peuvent non seulement s'exécuter côté client, sur le navigateur, mais aussi côté serveur. Ce à l'aide d'interpréteurs comme `Node` ou `Deno`. Utiliser le même langage côté client et côté serveur possède plusieurs avantages :

- **factoriser** des parties de codes utilisés à la fois côté client et côté serveur.

- **déplacer** aisément des calculs du côté serveur vers le côté client et inversement.

- évite d'avoir à apprendre et **maîtriser deux langages** différents.

Dans le cadre de ce module, nous utiliserons `Deno` qui permet d'exécuter directement des fichiers TS sans avoir à générer des fichiers JS via `tsc`. Il a aussi l'avantage de supporter les APIs fournis par le navigateur.

Nous étudierons plus en détails `Deno` dans le cours suivant.

L'exécution d'un script TypeScript avec Deno se fait via la commande suivante :

```bash
deno run --check --watch $FILE
```

💡 L'option `--watch` permet de relancer votre application lorsque vous en modifiez un des fichiers.

💡 Vous pouvez aussi ajouter un shebang au début du fichier Typescript afin de pouvoir l'exécuter comme une commande shell :

```typescript
#!/usr/bin/env -S deno run --check
// Usage: ./index.ts [arg1,...]

Deno.args // arguments de la commande shell.
const answer = prompt($QUESTION); // affiche une invite à l'utilisateur.
```

📖 Il est de bon usage d'intégrer les lignes de commandes pour construire et exécuter un projet soit dans un fichier `README.md` à la racine du projet, soit en commentaire au début du fichier principal (e.g. `index.ts` / `main.ts` ).

💡 Comme avec `tsc`, il est recommandé d'utiliser un fichier de configuration pour les gros projets `deno.json`:

```json
{
  "tasks": {
    "taskname": "command",
    "run": "deno run --check --watch index.ts"
  }
}
```

```shell
deno task run # Exécuter "tâche "run".
```

#### Installation de Deno

```shell
DENO_INSTALL="/home/IUT/$USER/scratch/.deno"
curl -s https://deno.land/x/install/install.sh | sh
echo "export DENO_INSTALL='$DENO_INSTALL'" >> ~/.bashrc
echo 'PATH="$PATH:$DENO_INSTALL/bin"'
```

📖 Pour plus d'indications sur l'installation de Deno, vous pouvez vous reporter à :

https://docs.deno.com/runtime/manual/getting_started/installation

## Types et structures de données natives

### Déclaration d'une variable

Contrairement à Python, JS possède 3 manières de déclarer une variable :

```javascript
let   a = ...; // "block-scoped".
var   a = ...; // "function-scoped".
const A = ...; // une constante, i.e. ne peut être modifiée.
```

*Note:* contrairement à Python, JS n'a pas besoin du mot clef `global` pour accéder à une variable globale. L'object `globalThis` contient l'ensemble des variables globales.

### Types de bases

JavaScript distingue 6 types de base :

| Déclaration          | JS Type     | Python Type  |
| -------------------- | ----------- | ------------ |
| `let a = 42;`        | `number`    | `float`      |
| `let a = true;`      | `boolean`   | `bool`       |
| `let a = "str";`     | `string`    | `str`        |
| `let a = undefined;` | `undefined` | ≈ `NoneType` |
| `let a = {a: 42}`    | `object`    | ≈ `dict`     |
| `let a = foo;`       | `function`  | `function`   |

Il existe plusieurs valeurs spéciales :

| Valeur                     | JS Type     | En Python         | Remarques                       |
| -------------------------- | ----------- | ----------------- | ------------------------------- |
| `NaN`                      | `number`    | `float('nan')`    | Not a Number (e.g. 0/0)         |
| `Number.POSITIVE_INFINITY` | `number`    | `float('inf')`    |                                 |
| `Number.NEGATIVE_INFINITY` | `number`    | `float('-inf')`   |                                 |
| `0b01010111`               | `number`    | `0b01010111`      | Nombre en binaire.              |
| `0o777`                    | `number`    | `0o777`           | Nombre en octal                 |
| `0xFF`                     | `number`    | `0xFF`            | Nombre en hexadécimal           |
| `1.2e2`                    | `number`    | `1.2e2`           | Nombre en notation scientifique |
| `true`                     | `boolean`   | `True`            |                                 |
| `false`                    | `boolean`   | `False`           |                                 |
| `` `val: ${value}` ``      | `string`    | `f"val: {value}"` | littéraux de gabarits           |
| `null`                     | `object`    | ≈ `None`          | valeur "pas de valeurs"         |
| `undefined`                | `undefined` | ≈ `None`          | valeur inexistante              |

Pour retrouver le type d'une variable, vous pouvez utiliser `typeof $VAR`, qui est l'équivalent de `type($VAR)` en Python.

### Opérateurs

JavaScript utilise les mêmes opérateurs que Python à quelques exceptions près.

#### Opérateurs bit à bits :

| Opérateur | Nom         |
| --------- | ----------- |
| `a\|b`    | OU binaire  |
| `a&b`     | ET binaire  |
| `a^b`     | XOR binaire |
| `~a`      | NOT binaire |

#### Opérateurs logique

| JavaScript | Python    | Commentaires          |
| ---------- | --------- | --------------------- |
| `a\|\|b`   | `a or b`  | OU logique            |
| `a&&b`     | `a and b` | ET logique            |
| `!a`       | `not a`   | NON logique           |
| `a??b`     |           | Coallescence des nuls |

💡 *Astuce :* l'opérateur de coallescence des nuls peut être utilisé pour indiquer des valeurs par défauts :

```javascript
let a ??= b; // si a est null ou undefined, a = b.
let a ||= b; // si a est évalué à false, a = b
```

#### Opérateurs de comparaison

| JavaScript | Python       | Commentaires                 |
| ---------- | ------------ | ---------------------------- |
| `a == b`   | `a == b`     | égalité de valeur (à éviter) |
| `a != b`   | `a != b`     |                              |
| `a === b`  | `a is b`     | égalité stricte              |
| `a !== b`  | `a is not b` |                              |

Opérateurs communs : `>`, `>=`, `<`, `<=`.

#### Opérateurs arithmétiques

Opérateurs communs : `+` (binaire et unaire), `-` (binaire et unaire), `/`, `%`, `*`, `**`

| JavaScript | Python | Commentaires                       |
| ---------- | ------ | ---------------------------------- |
| `++a`      | `a+=1` | incrément                          |
| `a++`      |        | retourne la valeur puis incrémente |
| `--a`      | `a-=1` | décrément                          |
| `a--`      |        | retourne la valeur puis décrémente |

#### Opérateurs d'affectations

Comme en python, il s'agit des opérateurs arithmétiques suffixé d'un `=`.

Par exemple `a += b` est équivalent à `a = a+b`.

#### Conversions

```javascript
let a =  +$VALUE;  // number
let a = !!$VALUE;  // boolean
let a = (5).toString(); // string "5"
let a = (5).toString(2);// string "101" 
```

📖 `0`, `""`, `null`, `undefined`, sont évalués à `false`.

### Structures de données <mark>[TODO]</mark>

Contrairement à Python, JavaScript ne possède qu'un seul type de tableaux, les `Array` (l'équivalent des `list` en Python). Les dictionnaires quant à eux sont implémentés soit par des `Object` (si les clefs sont des `string`) ou par des `Map`.

| JavaScript | Python             | Remarques         |
| ---------- | ------------------ | ----------------- |
| `Array`    | `list` ou `tupple` |                   |
| `Object`   | `dict`             | Si clefs `string` |
| `Map`      | `dict`             |                   |
| `Set`      | `set`              |                   |

#### Opérateurs sur les objets et tableaux

| JavaScript               | Python           | Remarques                                      |
| ------------------------ | ---------------- | ---------------------------------------------- |
| `0 in a`                 |                  | Existence de la clé "0" dans l'objet/tableau a |
| `a[0]`                   | `a[0]`           | Valeur associé à la clé "0"                    |
| `delete a[0]`            | `del a[0]`       | Supprime un élément.                           |
| `a?.[0]`                 |                  | Chaînage optionnel (clé)                       |
| `a?.foo`                 |                  | Chaînage optionnel (membre)                    |
| `a?.()`                  |                  | Chaînage optionnel (appel fonction)            |
| `[...a, ...b]`           | `(*a, *b)`       | Spread operator (tableaux)                     |
| `{...a, ...b}`           | `{**a, **b}`     | Spread operator (objects)                      |
| `[a, b, ...c] = [1,2,3]` | `a,b,*c=(1,2,3)` | Décomposition (tableaux)                       |
| `{a, b, ...c} = {...}`   |                  | Décomposition (objects)                        |
| `{a: c}  = {a: 4}`       |                  | Décomposition et renommage (`c=4`).            |

#### Opérations

##### Array

##### Objects

+ Object.create(null)

##### Map

##### Set

##### String

## Indications de type avec Typescript

### Indication explicite de type

```typescript
let a = 42; // TS déduit que a est un number.
let a: number = 42; // on indique explicitement le type.
```

### Union de types

Il arrive qu'une variable puisse prendre des valeurs de plusieurs types différents :

```typescript
let a = 42; // TS déduit que a est un number.
a = "42"; // erreur, a ne peut être qu'un number.
let a: number | string = 42;
a = "42"; // légal
```

### Valeur optionnelle

Il arrive qu'une variable puisse ne pas avoir de valeurs à l'initialisation :

```javascript
let a: number; // erreur.
let a: number|undefined; // légal
```

### Types TypeScript pour les structures de données

| JavaScript       | Type TS                              | Remarques                                      |
| ---------------- | ------------------------------------ | ---------------------------------------------- |
| `Array`          | `Array<T>` ou `T[]`                  | Liste                                          |
| `Array`          | `[string, number]`                   | Tupple                                         |
| `Array`          | `ReadonlyArray<T>` ou `readonly T[]` | Liste en lecture seule.                        |
| `{v: 4}`         | `Record<string, number>`             | Liste associative (accepte de nouvelles clés). |
| `{v: 4, u: 5}`   | `Record<"v"\|"u", number>`           | Liste associative (clés précises).             |
| `o = {v: 4}`     | `Record<keyof typeof o, number>`     | Liste associative (clés précises).             |
| `{v: 4, u: "s"}` | `{v: number, u: string}`             | Liste associative (clés et valeurs précises).  |
| `Set`            | `Set<V>`                             |                                                |
| `Map`            | `Map<K,V>`                           |                                                |

### Alias de types

En TypeScript, il est possible de définir des alias pour désigner un type. Cela est bien utile lorsque la description du type est longue à écrire, pour expliciter la fonction du type, permettre de changer le contenu d'un type sans avoir à modifier l'intégralité du code, ou respecter l'encapsulation.

```typescript
type X = number | string;
let a: X = 42; // équivaut à let a: number | string = 42;


type Point2D = [number, number];  // explicite 
type Result  = { value: string }; // la structure peut changer.
type ID      = ???; // je peux utiliser le type ID sans en connaître le type exact.
```

### TS typeof

⚠ Il existe 2 `typeof`, un JS qui déduit le type de la **valeur** à l'exécution, et un TS qui, dans les indications de types, indique le type TS de la **variable**.

```typescript
type A = typeof a;     // TS, number | string
console.log(typeof a); // JS, string
```

## Structures de contrôle

### Conditions

```javascript
// [JS] Javascript

if( $COND2 ) {

} else if($COND2) {

} else {

}
```

```python
# [🐍] Python
if $COND1:
    pass
elif $COND2
    pass
else
    pass
```

### Opérateur ternaire

```javascript
// [JS] Javascript

let a = $COND ? $IF_TRUE : $IF_FALSE
let a = $COND
          ? $IF_TRUE
          : $IF_FALSE
```

```python
# [🐍] Python

a = $IF_TRUE if $COND else $IF_FALSE
```

### Switch

```javascript
// [JS] Javascript
switch(value) {
    case 'value':
        //...
        break;
    default:
        //...   
}
```

```python
# [🐍] Python
match value:
    case 'value':
        pass
    case _:
        pass
```

### Boucles

#### While

```javascript
// [JS] Javascript
while($COND) {
}
```

```python
# [🐍] Python
while $COND:
    pass
```

#### Do while

```javascript
// [JS] Javascript
do {
    // s'exécute au moins une fois.
} while( $COND );
```

#### For in (parcours les clefs)

```javascript
// [JS] Javascript
for( let value in $OBJ ) {
}
```

#### For of (parcours les valeurs)

```javascript
// [JS] Javascript
for( let value of $OBJ ) {
}
```

### Exceptions

```javascript
// [JS] Javascript
try {
    throw new Error($MESSAGE)
} catch(error) {

} finally {

}
```

```python
# [🐍] Python
try:
    raise Exception($MESSAGE)
except Exception as error:
    pass
finally:
    pass
```

## Fonctions

### Déclaration

Contrairement à Python, il n'est pas possible, en JavaScript, d'appeler une méthode avec le nom des paramètres (e.g. `foo(a=2);`), l'appel ne se fait qu'avec la position des paramètres (e.g. `foo(1,2,3)`).

```javascript
// [JS] JavaScript
function foo(arg1, arg2 = "2") {
    //...
    return 2.3;
}
foo(1)
```

```python
# [🐍] Python
def foo(arg1, arg2 = "2", /): # les paramètres positionnel uniquement
    # ...
    return 2.3

foo(1)
```

```typescript
//[TS] TypeScript
function foo(arg1: number, arg2?: string = "2"): number {
    // ...
    return 2.3;
}

foo(1)
```

### Paramètre du reste (rest parameter)

```javascript
// [JS] JavaScript
function foo(arg1, ...arg2) {
    // arg2 = ["2", "3"]
    return 2.3;
}
foo(1, "2", "3")
```

```python
# [🐍] Python
def foo(arg1, /, *arg2):
    # arg2 = ("2", "3")
    return 2.3

foo(1, "2", "3")
```

```typescript
//[TS] TypeScript
function foo(arg1: number, ...arg2: readonly string[]): number {
    // arg2 = ["2", "3"]
    return 2.3;
}

foo(1, "2", "3")
```

### Opérateur de décomposition (spread operator) comme argument

```javascript
// [JS] JavaScript
function foo(arg1, arg2, arg3) {
    //...
    return 2.3;
}
const args = ["2", "3"];
foo(1, ...args)
```

```python
# [🐍] Python
def foo(arg1, arg2, arg3, /):
    # ...
    return 2.3

args = ("2", "3")
foo(1, *args )
```

```typescript
//[TS] TypeScript
function foo(arg1: number, arg2: string, arg3: string): number {
    return 2.3;
}
const args = ["2", "3"] as const; // ou as ["string", "string"]
foo(1, ...args)
```

### Generiques

En Typescript, un générique est désigné par `<T>`.

Par exemple prenons une fonction `min(a, b)` qui retourne la valeur minimale entre `a` et `b`. Ils peuvent être de n'importe quel type, cependant, `a`, `b` et la valeur de retour doivent être du même type. Ainsi on écrira :

```typescript
function min<T>(a: T, b: T): T { ... }


min<number>(1,2); // ne peut retourner qu'un number.
min(1,2); // aux paramètres, TS déduit que T = number.
```

Si on veut restreindre les types possibles pour `T`, on utilisera `extends` :

```typescript
function min<T extends string|number|boolean>(a: T, b: T): T { ... }
```

### Asynchronisme

L'asynchronine permet d'éviter de bloquer le thread principal lors d'opérations bloquantes et coûteuses. Par exemple, de ne pas bloquer la GUI et effectuer d'autres opérations en attendant le retour d'un `fetch()` qui peut prendre quelques ms à plusieurs secondes.

Comme Python, JavaScript gère l'asynchronisme, à une différence près. En JavaScript, la fonction asynchrone commence à s'exécuter dès son appel et retourne une promesse. En Python, l'appel à la fonction asynchrone retourne directement une coroutine, et la fonction ne commence à s'exécuter que lorsque la coroutine est soit attendue, soit donnée à une "boucle" asyncio.

| TypeScript                | Python             | Remarques                                      |
| ------------------------- | ------------------ | ---------------------------------------------- |
| Promesse                  | Coroutine          | Mécanisme                                      |
| `async function foo() {}` | `async def foo():` | Déclarer une fonction asynchrone               |
| `async foo(){...}`        | `async def foo():` | Déclarer une méthode asynchrone                |
| `await foo()`             | `await foo()`      | Attendre le résultat d'une fonction asynchrone |

En réalité `async function foo(): number {...}` est un sucre syntaxique pour écrire `function foo(): Promise<number> { return new Promise(() => {...}) }`.

Une promesse s'utilise comme suit :

```typescript
const p = new Promise<number>( (resolve, reject) => {
    // code à exécuter de manière asynchrone.
    resolve($RETURN_VALUE); // await p retournera $RETURN_VALUE
    reject($MESSAGE);        // await p lancera une exception.
});
//...
await p;
// ou
let {promise, resolve, reject} = Promise.withResolvers<number>();
addEventListener($EVENT, () => resolve(), {once: true});
// la promesse sera résolue plus tard.
await promise;

// attendre plusieurs promesses (retourne un tableau des résultats)
await Promise.all([p1, p2,..]); // exception si une des promesses échoue.
await Promise.allSettled([p1, p2, ...]); // tableau de {status, value, reason}


// attendre la première promesse
await Promise.race([p1, p2, ...); // s'arrête au premier resolve ou reject.
await Promise.any([p1, p2, ...]); // s'arrête au premier resolve.
```

## Futur

- Manipuler URL => move 2 JS-DOM API ?

- console/assert => move 2 JS-DOM API ?
