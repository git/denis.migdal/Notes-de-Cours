Afin de fournir une interface graphique permettant d'administrer votre serveur, vous allez concevoir, puis implémenter, un site Web qui requêtera l'API REST que vous avez créé à la séance précédente afin d'interagir avec le serveur.

## Prise en main de LISS (20min)

Pour cela vous allez utiliser une bibliothèque développée par mes soins afin de vous simplifier le travail.

1. Clonez la bibliothèque LISS dans le dossier `/libs/LISS` :
   `git submodule add git@github.com:denis-migdal/LISS.git ./libs/LISS` .

2. Installez `tsc`, puis créez un fichier `/client/tsconfig.json`.

3. Créez un dossier `/client/components/` qui contiendra vos différents composants Web.

4. Créez votre première page `/client/index.html` contenant votre premier composant :
   
   - Balise : `<hello-world name='$name'></hello-world>`
   
   - Affichage : `Bonjour $name`

5. Créez un fichier `/client/README.md` à compléter au fur et à mesure.

## Conception (50min)

Dans un premier temps, vous réfléchirez aux différents composants que votre serveur devra fournir. Pour cela, vous rédigerez un document Markdown dans `/client/doc/conception.md`. Vous devrez ainsi définir les différents composants de votre page Web, avec :

- leur interface graphique :
  
  - leur contenu graphique.
  
  - les intéractions utilisateurs, e.g. action lors d'un clic sur un élément.

- l'API offerte par vos composants :
  
  - ses méthodes publiques.
  
  - les événements qu'il lance.

- les requêtes REST qu'ils doivent effectuer.

Par exemple :

**Composants :**

- `<user-list> / UserList` : liste des utilisateurs (tableau).
  
  - `<tr is='user-line'> / UserLine` : ligne de tableau représentant un utilisateur.
    - cellule "nom"
    - cellule "supprimer" (clic => `this.delete()`)

💡 Il est recommandé de représenter cela sous la forme d'un schéma, permettant de se faire une idée de la structure générale de la page finale. Il est potentiellement plus aisé le le faire au crayon à papier puis de scanner le schéma :![]()

![schéma.png](/home/demigda/Data/Recherche/Git/Notes-de-Cours/Web/PW/TP3%20-%20Client%20Web/schéma.png)

**APIs :**

- **UserLine :**
  
  ```ts
  /*
  Events:
      - DELETED: (user: UserLine) => void
  */
  class UserLine {
      delete(): void;
  }
  ```

**Processus :**

- **Supprimer un utilisateur (GUI) :**
  Clic sur cellule "supprimer" de `<user-line>` -> `UserLine.delete()` -> `DELETE /users/{id}` -> `UserLine.dispatchEvent(DELETED)` -> `UserList.removeLine(line: UserLine)`.

💡 Vous pouvez représenter certains processus sous la forme d'un diagramme de flux :

![PW TP3.png](/home/demigda/Data/Recherche/Git/Notes-de-Cours/Web/PW/TP3%20-%20Client%20Web/PW%20TP3.png)

💡 Pour éviter d'y passer trop de temps, vous pouvez vous contenter d'un tableau :

| Processus                        | Action                                     | Méthode             | Requête REST          | Événement |
| -------------------------------- | ------------------------------------------ | ------------------- | --------------------- | --------- |
| Suppression<br/>d'un utilisateur | Clic sur<br/>«delete» de<br/>`<user-line>` | `UserLine.delete()` | `DELETE /users/${id}` | `DELETED` |

💡 Vous pouvez utiliser les outils suivants :

- Diagrammes : [LucidCharts](https://lucidcharts.com/) ou Dia.

- Schémas plus avancés : Inkscape (mais potentiellement chronophage)

- Markdown : MarkText

## Implémentez votre site Web (50min)

Vous implémenterez votre site Web en vous basant sur le document de conception que vous aurez produit.

## Attentes

- [ ] Utilisation de git.

- [ ] Présence et qualité des fichiers de documentations (`client/doc`, `client/README.md`).

- [ ] Qualité de la conception.

- [ ] Au moins 1 à 2 composants Web implémentés avec LISS en TypeScript.

- [ ] Qualité du code.
