Vous allez concevoir puis implémenter un serveur HTTP fournissant une API REST permettant de gérer votre serveur à distance.

## Prise en main de VSHS (20min)

Pour cela vous allez utiliser une bibliothèque développée par mes soins afin de vous simplifier le travail.

1. Créez un nouveau projet git : `git init`.

2. Clonez la bibliothèque VSHS dans le dossier `/libs/VSHS` :
   `git submodule add git@github.com:denis-migdal/VSHS-TS-.git ./libs/VSHS` .

3. Démarrez le serveur d'exemple, puis effectuez une requête (lisez la documentation).

4. Créez un fichier `/server/deno.json`, ainsi qu'un dossier `/server/routes/`.

5. Créez un fichier `/server/index.ts` permettant de lancer votre propre serveur.

6. Créez, puis testez, votre première route :
   
   - Requête : `GET /hello-world?name=$name`
   
   - Réponse : `Hello $name`
   
   ⚠ Pensez à relancer votre serveur lorsque vous ajoutez une nouvelle route.

7. Créez un fichier `server/README.md` à compléter au fur et à mesure, et contenant, entres autres, la commande pour lancer et tester le serveur.

## Conception (30min)

Dans un premier temps, vous réfléchirez aux différentes fonctionnalités que votre serveur devra fournir. Pour cela, vous rédigerez un document Markdown dans `server/doc/conception.md`. Vous devrez ainsi définir :

- les routes

- les structures des données

- les requêtes REST en précisant :
  
  - le format de la requête : méthode HTTP, route, paramètres (dans url, corps, route).
  
  - le format de la réponse.
  
  - les données exploitées et leur source : fichier, BDD, RAM, commande shell.

Par exemple :

**Routes :**

- `/users/` : liste des utilisateurs.
  
  - `/users/{id}` : utilisateur `$id`.

**Structures des données :**

- **Utilisateur :**
  
  ```ts
  {
      name: string,
      email: string
  }
  ```

**API :**

- `/hello-world`
  
  - `GET /hello-world?name=$name`
    
    - *Description :* route permettant de tester le bon fonctionnement du serveur.
    
    - *Réponse :* `Bonjour $name.`
    
    - *Données :* pas de données.

💡 Vous pouvez utiliser les outils suivants :

- Diagrammes : [LucidCharts](https://lucidcharts.com/) ou Dia.

- Schémas plus avancés : Inkscape (mais potentiellement chronophage)

- Markdown : MarkText

## Implémentez votre serveur (1h)

Vous implémenterez votre serveur en vous basant sur le document de conception que vous aurez produit.

Dans un premier temps, vous implémenterez un `dry mode` (par opposition au `live mode`), permettant de simuler l'exécution d'une requête sans réellement appliquer les changements demandés (exécution à sec).

Le `dry mode` est très utilisé dans le cadre de tests, e.g., pour tester l'envoi d'une requête, et le traitement de sa réponse par une application cliente. L'avantage est ainsi d'effectuer des suites de tests sans réellement modifier l'état du serveur. Le `dry mode` est aussi utilisé par des commandes shells, e.g. `rsync --dry-run` ou  `dpkg --dry-run`, afin de s'assurer que les paramètres de la commande produisent bien l'effet escompté avant de réellement l'exécuter.

Pour implémenter le `dry mode`, vous créerez des interfaces qui seront implémentées par des classes, e.g. :

```ts
enum Mode {
    DRY,
    LIVE
}

interface IUserManager {
    create(name: string): IUser;
}

class LiveUserManager implements IUserManager {
    override create(name: string) { /* live implementation */ }
}
class DryUserManager implements IUserManager {
    override create(name: string) { /* dry implementation */ }
}

const dryUserManager  = new DryUserManager ();
const liveUserManager = new LiveUserManager();
export function getUserManager(mode: Mode = Mode.LIVE) {
    return mode === Mode.LIVE ? liveUserManager : dryUserManager;
}
```

```ts
export default function() {
    const userManager = getUserManager(Mode.DRY);

    // Pas de différences d'usage entre le mode dry et le mode live.
    let user = userManager.create("toto");
    user.getName();
    //...
}
```

### [Avancé] Live mode

Si vous avez le temps, vous pourrez implémenter le mode `live` sur une VM.

Vous serez certainement amené à créer des scripts shells qui seront appelés par votre serveur HTTP. Vous les placerez alors dans le dossier `/server/scripts/`.

Les outils suivants pourront vous être utiles :

- `tail -F` pour écouter des fichiers de logs.
- inotifywait pour d'autres fichiers (<mark>TODO</mark>: commande exacte à retrouver + API Deno)

💡 Pour aisément passer d'un mode à l'autre, vous pouvez soit :

- préfixer vos routes par `live` ou `dry`, en fonction du mode souhaité.

- ajouter un paramètre `mode: "dry"|"live"` dans l'URL ou le corps de la requête.

## Attentes

- [ ] Utilisation de git.

- [ ] Présence et qualité des fichiers de documentations (`server/doc`, `server/README.md`).

- [ ] Qualité de la conception.

- [ ] 1 à 2 routes avec 4 méthodes HTTP pour chaque (récupérer, ajouter, modifier, supprimer), implémentées avec VSHS en TypeScript.

- [ ] Qualité du code.

- [ ] **[Avancé]** implémenter un Server-Sent Events pour donner, en temps réel, les différentes modifications survenant sur le serveur (e.g. un utilisateur a été créé).

- [ ] **[Avancé]** Implémenter le mode live.
