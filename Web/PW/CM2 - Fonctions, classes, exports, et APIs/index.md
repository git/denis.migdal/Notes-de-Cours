---
gitea: none

include_toc: true
---

# Classes et API

[TOC]

## Classes

### Usage

Bien que la syntaxe change quelque peu, les classes JS/TS ont un fonctionnement très similaires à celles de Python.

| TypeScript                | Python                          | Description                                             |
| ------------------------- | ------------------------------- | ------------------------------------------------------- |
| `class C{...}`            | `class C:`                      | Déclarer une classe                                     |
| `let c = new C()`         | `c = C()`                       | Instancier une classe                                   |
| `constructor(...){...}`   | `def __init__(self):`           | Constructeur/Méthode                                    |
| `_x: string`              | `_x = None`                     | Déclarer un attribut                                    |
| `_x?: string`             | `_x = None`                     | Déclarer un attribut non initialisé                     |
| `_x = "str"`              | `_x  = "str"`                   | Initialiser un attribut                                 |
| `this._x`                 | `self._x`                       | Accéder à un attribut à l'intérieur d'une méthode.      |
| `get x() {...}`           | `@property x(self):`            | Getter                                                  |
| `set x(value) {...}`      | `@x.setter def x(self, value):` | Setter                                                  |
| `this.x = 2`              | `self.x = 2`                    | Accéder à un getter/setter à l'intérieur d'une méthode. |
| `static foo = 42`         | `foo = 42`                      | Attribut statique                                       |
| `static foo(){...}`       | `@staticmethod def foo():`      | Méthode statique                                        |
| `C.foo`                   | `C.foo`                         | Accéder à un membre statique                            |
| `class C extends B {...}` | `def C(B):`                     | Héritage                                                |
| `this`                    | `self`                          | Auto-référence                                          |
| `super`                   | `super()`                       | Classe-mère                                             |

 📖 Les getters et setters peuvent être vus comme des attributs donc la valeur est retournée par le getter, et la modification gérée par le setter.

Il peuvent avoir plusieurs utilités :

- créer un attribut donc la valeur n'est pas stockée mais calculée ;

- créer un attribut accessible qu'en lecture ou qu'en écriture ;

- contrôler les valeurs possibles dans le setter.

```typescript
class C {
    #list = [];

    get size() {
        return this.#list.length;
    }    
    set size(size: number) {
        if(size < 0) throw new Error('Une taille doit être positive!');
        this.#list.length = size;
    }
}

const c = new C();
c.size = 2;          // setter
console.log(c.size); // getter
```

### Ajouts de Typescript

| TypeScript       | Javascript        | Python                  | Commentaires             |
| ---------------- | ----------------- | ----------------------- | ------------------------ |
| `protected name` | `_name`           | `_name`                 | Attribut protégé.        |
| `private name`   | `#name`           | `__name`                | Attribut privé.          |
| `readonly name`  | `get name(){...}` | `@property def name():` | Attribut non-modifiable. |

Typescript permet aussi de créer des **classes abstraites** ainsi que des **interfaces**. Toutes les deux possèdent des **membres abstraits**, i.e. qui devront être implémentées par la classé héritant/implémentant la classe abstraite/interface. L'interface ne possède que des membres abstraits, tandis que la classe abstraite peut posséder des membres non-abstraits.

| Classe Abstraite       | Interface                 | Remarques               |
| ---------------------- | ------------------------- | ----------------------- |
| `abstract class X{}`   | `interface X{}`           | Déclaration             |
| `class C extends X {}` | `class C implements X {}` | Héritage/implémentation |
| `abstract foo();`      | `foo();`                  | Membre abstrait         |

### Notions avancées [<mark>TODO</mark>]

#### Well-known Symbols

#### Mixins

#### Prototype

+ bind

## Import/export

Comme en Python, on peut exporter des symboles (fonctions, classes, variables, etc) et les importer dans un autre fichier afin de les utiliser. En Javascript et contrairement à Python, l'import utilise le chemin du fichier (e.g. `./toto/titi.ts`) et non un nom de module (e.g. `toto.titi`). Aussi les symboles à exporter doivent être explicités en leur préfixant le mot clef `export`.

| Typescript                      | Python                           | Remarques                                                 |
| ------------------------------- | -------------------------------- | --------------------------------------------------------- |
| `export class X{}`              | `def X:`                         | Déclarer une classe à exporter                            |
| `export function foo(){}`       | `def foo():`                     | Déclarer une fonction à exporter                          |
| `export const X = ...`          | `X = ...`                        | Déclarer une variable à exporter                          |
| `export type T = ...`           |                                  | Déclarer un type à exporter.                              |
| `import {X, foo} from '...'`    | `from '...' import X, foo`       | Importer des symboles                                     |
| `import * as M from '...'`      | `import '...' as M`              | Importer tous les symboles                                |
| `export default class X{}`      |                                  | Export par défaut                                         |
| `import X, {...} from '...'`    |                                  | Importer l'export par défaut.                             |
| `const M = await import('...')` | `importlib.import_module('...')` | Import dynamique.                                         |
| `export {...} from '...'`       |                                  | Exporter des symboles d'un autre fichier                  |
| `export {X as Y} from '...'`    |                                  | Exporter sous un autre nom un symbole d'un autre fichier. |
| `import type {...} from '...'`  |                                  | Importer en tant que type Typescript.                     |

### Import maps

Les cartes d'imports permettent de définir des alias pour les imports :

```html
<!-- Côté client : fichier HTML -->
<!DOCTYPE html>
<html>
    <head>
        <script type="importmap">
        {
            "imports": {
                "$NAME": "$PATH"
            }
        }
        </script>
        <script type="module">
          import * from '$NAME/...'; // équivalant à from "$PATH/..."
        </script>
    </head>
</html>
```

```json
// Côté serveur : deno.json
{
  "imports": {
    "/": "./",
    "./": "./",
    "$NAME/": "$PATH/"
  },
  "tasks": {
     //...
  }
}
```

⚠ Les imports map sont actuellement encore boguées.

## Fonctions (avancé)

### Fonctions génératrices

<mark>TODO: utilité</mark>

```javascript
// [JS] JavaScript
function * foo(nb) {
    for(let i = 0; i < nb; ++i)
        yield i;
}

for( let val of foo(5) )
    console.log(val);
```

```python
# [🐍] Python
def foo(nb, /) {
    for i in range(nb):
        yield i;
}

for val in foo(5)
    console.log(val);
```

```typescript
//[TS] TypeScript
function * foo(nb: number): Generator<number> {
    for(let i = 0; i < nb; ++i)
        yield i;
}

for(let val of foo(5) )
    console.log(val);
```

### Fonctions fléchées

<mark>TODO</mark>: pas d'équivalent en python + utilité (callbacks)

```javascript
const foo = arg => arg[0]; // est équivalent à :
const foo = function(arg){ return arg[0]; }

// si plusieurs paramètres, utiliser des parenthèses ().
// si valeur retournée plus complexe, utiliser des accolades {}.
const foo = (arg1, args) => { ... ; return x; } // est équivalent à :
const foo = function(arg1, arg2){ ... ; return x; }
```

## API Deno

File (cf 54&55 old slides CM1)

BDD (cf 56&57 old slides CM1)

Server (cf lien) / npm

## API JS

Date / Regexp / Math API / Crypto
